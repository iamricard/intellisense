const { compose, flatten, map, prop, head, last } = require('ramda')
const { Axes, Components, Scales, Plots, Dataset } = require('plottable')
const regression = require('regression')

const inputData = require('./data.json')

const domains = map(prop('source'), inputData)
const YValues = compose(
  flatten,
  map(map(last)),
  map(prop('values'))
)(inputData)
const YDomain = [Math.min(...YValues), Math.max(...YValues)]

const scales = {
  x: new Scales.Category(),
  y: new Scales.Linear().domain(YDomain),
  color: new Scales.Color().domain(domains)
}
const legend = new Components.Legend(scales.color).maxEntriesPerRow(Infinity)
const axes = {
  x: new Axes.Category(scales.x, 'bottom'),
  y: new Axes.Numeric(scales.y, 'left')
}
const generateRegressions = map(({ source, values }) => ({
  source,
  values: regression('polynomial', values, 4).points
}))

const data = {
  trends: generateRegressions(inputData),
  scatter: inputData
}

const scatterPlot = ({ source, values }) =>
  new Plots.Scatter()
    .addDataset(new Dataset(values))
    .x(head, scales.x)
    .y(last, scales.y)
    .attr('stroke', source, scales.color)
    .attr('fill', source, scales.color)

const trendLine = ({ source, values }) =>
  new Plots.Line()
    .addDataset(new Dataset(values))
    .x(head, scales.x)
    .y(last, scales.y)
    .attr('stroke', source, scales.color)
    .attr('fill', source, scales.color)
    .attr('opacity', 0)

const scatterPlots = map(scatterPlot, data.scatter)
const trendPlots = map(trendLine, data.trends)

const plots = new Components.Group([...scatterPlots, ...trendPlots])

const table = new Components.Table([
  [null, legend],
  [axes.y, plots],
  [null, axes.x]
])

table.renderTo('svg#main')

let hideTrends = true

document
  .getElementById('trends')
  .addEventListener('click', (evt) => {
    evt.preventDefault()
    hideTrends = !hideTrends

    trendPlots.map((plot) => {
      plot.attr('opacity', () => {
        return hideTrends ? 0 : .5
      })
    })
  })
