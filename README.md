# IntelliSense

## Summary

This project uses three libraries:

 * ramda for control flow
 * regression-js to calculate regression curves
 * plottable to generate d3-backed charts

It also uses `basscss` for styling and `budo` to serve a browserified script.

It uses data provided in `data.json` to render scatter plots for each dataset
and then provide regression curves (fourth polynomial) to see trends.

Note: The trends look a little bit funky because the data is completely random!

## Usage

```sh
npm i
npm start # navigate to localhost:9966
```
